class JobApplication < Sequel::Model
  VALID_FORMAT = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/

  attr_accessor :applicant_email
  attr_accessor :job_offer

  one_to_one :job_offers
  one_to_one :users

  def validate
    super
    validate_email(:applicant_email)
  end

  def validate_email(email)
    expresion = VALID_FORMAT
    result = email.to_s.match(expresion)
    !result.nil?
  end

  def self.create_for(email, offer)
    app = JobApplication.new
    app.applicant_email = email
    app.job_offer = offer
    app
  end

  def self.find_by_job_offer(job_offer)
    JobApplication.where(job_offer_id: job_offer)
  end

  def process
    JobVacancy::App.deliver(:notification, :contact_info_email, self)
  end
end
