class JobOfferQuestion
  attr_reader :from
  attr_reader :to
  attr_reader :subject
  attr_reader :question
  attr_reader :dont_send

  def initialize(p_from, p_to, p_subject, p_question)
    @dont_send = p_question.nil? || p_question.strip.empty?
    @from = p_from
    @to = p_to
    @subject = p_subject
    @question = p_question
  end

  def send_mail
    return if @dont_send

    JobVacancy::App.deliver(:offer_question, :offer_question_email, self)
  end
end
