require_relative './password_validator'
require_relative './exception_missing_data'
require_relative './exception_password_doesnt_match'

class RegistrationValidator
  attr_accessor :registers_params, :confirmation

  def validate(received_params, password_confirmation)
    @registers_params = received_params
    @confirmation = password_confirmation
    @password_validator = PasswordValidator.new
    @password_validator.validate(@registers_params[:user][:password])
    @birthdate_validator = BirthdateValidator.new
    @birthdate_validator.validate(@registers_params[:user][:birthdate])
    match_password?
    has_all_fields?
  end

  def match_password?
    password = @registers_params[:user][:password]
    exception_message = 'Passwords do not match'
    raise ExceptionPasswordDoesntMatch, exception_message unless password == @confirmation

    true
  end

  def has_all_fields?
    @user_for_validate = User.new(registers_params[:user])
    exception_message = 'All fields are mandatory'
    raise ExceptionMissingData, exception_message unless @user_for_validate.valid?

    true
  end
end
