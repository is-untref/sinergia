require_relative '../modules/date_utils'
require 'date'

class User < Sequel::Model
  include DateUtils
  one_to_many :job_offers
  one_to_many :job_applications

  LIMIT_MINUTES_FOR_RESET_PASSWORD = 10

  def validate
    super
    validates_presence %i[name email crypted_password short_bio]
    validates_format(/\A[^@\s]+@([^@\s]+\.)+[^@\s]+\z/, :email)
    validate_short_bio(:short_bio)
  end

  def password=(password)
    self.crypted_password = ::BCrypt::Password.create(password) unless password.nil?
  end

  def generate_token(email)
    user = User.first(email: email)
    user.token = rand(36**8).to_s(36)
    user.updated_on = Time.zone.now
    user.save
    user.token
  end

  def verify_time?
    message_error = 'The time for restore password has expired.'
    a_last_update = DateTime.parse(updated_on.strftime('%Y-%m-%d %H:%M:%S'))
    time_ago = DateTime.parse(LIMIT_MINUTES_FOR_RESET_PASSWORD
                      .minutes.ago.strftime('%Y-%m-%d %H:%M:%S'))
    raise StandardError, message_error if a_last_update < time_ago
  end

  def send_url_restore_password
    JobVacancy::App.deliver(:restore_password, :contact_info_email, self)
  end

  def self.authenticate(email, password)
    user = User.first(email: email)
    raise StandardError, 'Your account has been blocked for 24 hours' if user&.is_blocked?(user)
    raise StandardError, 'Invalid credentials' unless user&.has_password?(password)

    user
  end

  def is_blocked?(user)
    return false if user.login_tries.to_i < 6

    if user.block_date.nil?
      to_block(user, Time.zone.now)
      true
    elsif is_less_than_24?(user.block_date)
      true
    else
      to_block(user)
      user.login_tries = 0
      user.save
      false
    end
  end

  def has_password?(password)
    ::BCrypt::Password.new(crypted_password) == password
  end

  private

  def validate_short_bio(a_short_bio)
    a_short_bio.length <= 500
  end

  def is_less_than_24?(a_block_date)
    count_different_hour(a_block_date) <= 24
  end

  def to_block(user, value = nil)
    user.block_date = value
    user.save
  end
end
