require 'active_support/all'

class BirthdateValidator
  attr_accessor :date

  def validate(date_param)
    @date = Date.parse(date_param)
    future_date?
    too_young?
  end

  def future_date?
    exception_message = 'Birthdate cannot be in the future'
    raise ExceptionFutureBirthdate, exception_message if @date.to_datetime.future?

    true
  end

  def too_young?
    exception_message = 'You must be at least 15 years of age'
    raise ExceptionYoungUser, exception_message unless ((Date.today - @date) / 365) > 15

    true
  end
end
