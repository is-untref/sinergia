require_relative './exception_password_without_numbers'
require_relative './exception_short_password'
require_relative './exception_password_without_uppercase'
require_relative './exception_password_without_lowercase'
require_relative './exception_password_without_symbols'

class PasswordValidator
  def validate(password)
    validate_long(password)
    validate_numbers(password)
    validate_lowercases(password)
    validate_uppercases(password)
    validate_symbols(password)
  end

  def validate_long(password)
    exception_message = 'The password must be at least 12 characters'
    raise ExceptionShortPassword, exception_message unless password.length > 11

    true
  end

  def validate_numbers(password)
    exception_message = 'The password must constains at least one number character'
    raise ExceptionPasswordWithoutNumbers, exception_message unless password =~ /\d/

    true
  end

  def validate_lowercases(password)
    exception_message = 'The password must constains at least one lower case character'
    raise ExceptionPasswordWithoutLowercase, exception_message unless password =~ /[a-z]/

    true
  end

  def validate_uppercases(password)
    exception_message = 'The password must constains at least one upper case character'
    raise ExceptionPasswordWithoutUppercase, exception_message unless password =~ /[A-Z]/

    true
  end

  def validate_symbols(password)
    exception_message = 'The password must constains at least one symbol'
    symbols = "?<>',?[]}{=-)(*&^%$#`~{}!@*.~|/+-_:;"
    symbols_regex = /[#{symbols.gsub(/./) { |char| "\\#{char}" }}]/
    raise ExceptionPasswordWithoutSymbols, exception_message unless password =~ symbols_regex

    true
  end
end
