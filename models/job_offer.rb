class JobOffer < Sequel::Model
  many_to_one :user
  one_to_many :job_applications

  BLANKS = /([^a-z]+\s)|([^a-z]\s[^a-z])|(\s+[^a-z])/
  CHARACTER_BLANKS = /\s+\s/
  START_END_BLANKS = /^\s|\s$/

  def validate
    super
    validates_presence :title
    validates_remuneration(:remuneration)
  end

  def before_save
    normalize_labels
    super
  end

  def normalize_labels
    a_label = old_value_labels.to_s.downcase
    a_label = a_label.gsub(CHARACTER_BLANKS, ' ')
    a_label = a_label.gsub(START_END_BLANKS, '')
    self.labels = a_label.gsub(BLANKS, ',')
  end

  def old_value_labels
    labels
  end

  def owner
    user
  end

  def owner=(a_user)
    self.user = a_user
  end

  def self.all_active
    JobOffer.where(is_active: true)
  end

  def self.find_by_owner(user)
    JobOffer.where(user: user)
  end

  def self.deactivate_old_offers
    JobOffer.all_active.each do |offer|
      if (Date.today - offer.updated_on) >= 30
        offer.deactivate
        offer.save
      end
    end
  end

  def activate
    self.is_active = true
  end

  def deactivate
    self.is_active = false
  end

  def validates_remuneration(value)
    errors.add(value, 'Only numbers between 1 and 999999') unless is_ok?(value)
  end

  def is_ok?(value)
    value.nil? || value.to_s.to_i < 1_000_000
  end
end
