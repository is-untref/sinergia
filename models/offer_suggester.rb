class OfferSuggester
  # rubocop: disable TrivialAccessors
  attr_accessor :last_suggest
  def self.get_suggest(job_offer)
    suggest_offers = []
    job_offer.labels.split(',').each do |label|
      label.split(' ').each do |label_splited|
        # rubocop: disable LineLength
        query_for_similars_offers = "SELECT job_offers.* FROM job_offers WHERE labels LIKE '%#{label_splited}%' AND is_active = true"
        similars_offers_to_label_splited = JobOffer.fetch(query_for_similars_offers).all
        suggest_offers.concat similars_offers_to_label_splited
        # rubocop: enable LineLength
      end
    end
    @last_suggest = suggest_offers.uniq.reject { |hash| hash[:id] == job_offer.id }
    @last_suggest
  end

  def self.last_suggest
    @last_suggest
  end

  def self.last_suggest=(val)
    @last_suggest = val
  end
  # rubocop: enable TrivialAccessors
end
