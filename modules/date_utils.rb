module DateUtils
  def count_different_hour(a_date, current = Time.zone.now)
    value = (current.to_time - a_date.to_time) / 3600.0
    value
  end
end
