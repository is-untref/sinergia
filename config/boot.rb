require 'active_support/time'

# Defines our constants
RACK_ENV = ENV['RACK_ENV'] ||= 'development' unless defined?(RACK_ENV)
# rubocop: disable LineLength
ENV_SITE_KEY = ENV['RECAPTCHA_SITE_KEY'] ||= '6LcuyXcUAAAAADpCAXDwXtdshzyziPvnNzqmKc74' unless defined?(ENV_SITE_KEY)
ENV_SECRET_KEY = ENV['RECAPTCHA_SECRET_KEY'] ||= '6LcuyXcUAAAAACHdGQplAAjZmpmR6YIlEBeNsLRn' unless defined?(ENV_SECRET_KEY)
# rubocop: enable LineLength
PADRINO_ROOT = File.expand_path('..', __dir__) unless defined?(PADRINO_ROOT)

# Load our dependencies
require 'bundler/setup'
Bundler.require(:default, RACK_ENV)

#
# # Enable devel logging
#
# Padrino::Logger::Config[:development][:log_level]  = :devel
# Padrino::Logger::Config[:development][:log_static] = true
#
# # Configure your I18n
#
# I18n.default_locale = :en
#
# # Configure your HTML5 data helpers
#
# Padrino::Helpers::TagHelpers::DATA_ATTRIBUTES.push(:dialog)
# text_field :foo, :dialog => true
# Generates: <input type="text" data-dialog="true" name="foo" />
#
# # Add helpers to mailer
#
# Mail::Message.class_eval do
#   include Padrino::Helpers::NumberHelpers
#   include Padrino::Helpers::TranslationHelpers
# end

#
# Add your before (RE)load hooks here
#
Padrino.before_load do
  Sequel::Model.plugin :validation_helpers
end

#
# Add your after (RE)load hooks here
#
Padrino.after_load do
  Time.zone = 'Buenos Aires'
  Time.zone_default = Time.zone
end

Padrino.load!
