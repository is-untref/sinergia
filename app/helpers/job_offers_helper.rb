# Helper methods defined here can be accessed in any controller or view in the application

JobVacancy::App.helpers do
  def job_applications
    @job_applications = JobApplication.find_by_job_offer(params[:offer_id])
    @job_applications
  end

  def get_short_bio(a_job_application)
    a_user = User.first(id: a_job_application[:user_id])
    return 'not specified' if a_user.nil?
    return 'not specified' if a_user.short_bio.nil?

    a_user.short_bio
  end

  def get_count_job_applications(a_id_offer)
    @job_applications = JobApplication.find_by_job_offer(a_id_offer)
    @job_applications.count
  end
end
