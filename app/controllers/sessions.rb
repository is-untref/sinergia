require 'recaptcha'
JobVacancy::App.controllers :sessions do
  get :login, map: '/login' do
    @user = User.new
    @show_captcha = false
    render 'sessions/new'
  end

  post :create do
    email = params[:user][:email]
    password = params[:user][:password]
    begin
      @user = User.authenticate(email, password)
      if @user&.login_tries&. > 3
        raise StandardError, 'Please check the captcha' unless verify_recaptcha
      end
    rescue StandardError => e
      @user = User.first(email: email)
      @user&.login_tries += 1
      @show_captcha = true if @user&.login_tries&. > 3
      if @user.nil?
        @user = User.new
      else
        @user.save
      end
      flash.now[:error] = e.message
      render 'sessions/new'
    else
      @user.login_tries = 0
      @show_captcha = false
      @user.save
      sign_in @user
      redirect '/'
    end
  end

  get :destroy, map: '/logout' do
    sign_out
    redirect '/'
  end
end
