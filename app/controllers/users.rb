JobVacancy::App.controllers :users do
  get :new, map: '/register' do
    @user = User.new
    render 'users/new'
  end

  post :generate_token do
    a_email = params[:user][:email]
    @user = User.first(email: a_email)
    if @user.nil?
      flash[:error] = 'Invalid user'
      redirect '/'
    end
    a_token = @user.generate_token(a_email)
    @user.token = a_token
    flash[:success] = 'Link for restore password sent by mail'
    @user.send_url_restore_password
    redirect '/'
  end

  get :edit, with: :token do
    @user = User.first(token: params[:token])
    if @user.nil?
      flash[:error] = 'The link to restore password is invalid, request again from the login page'
      redirect '/'
    end
    begin
      @user.verify_time?
    rescue StandardError => e
      flash[:error] = e.message
      redirect '/'
    else
      render 'users/restore_password'
    end
  end

  get :forgot, map: '/forgot_password' do
    @user = User.new
    @show_captcha = false
    render 'users/forgot_password'
  end

  post :create do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }

    @user = User.new(params[:user])

    @registration_validator = RegistrationValidator.new

    begin
      @registration_validator.validate(params, password_confirmation)
    rescue StandardError => e
      flash.now[:error] = e.message
      render 'users/new'
    else
      @user.save
      flash[:success] = 'User created'
      redirect '/'
    end
  end

  post :restore do
    password_confirmation = params[:user][:password_confirmation]
    params[:user].reject! { |k, _| k == 'password_confirmation' }
    @user = User.first(email: params[:user][:email])
    @registration_validator = PasswordValidator.new
    begin
      @registration_validator.validate(password_confirmation)
      @user.save
    rescue StandardError => e
      flash.now[:error] = e.message
      render 'users/restore_password'
    else
      @user.password = password_confirmation
      @user.token = nil
      @user.save
      flash[:success] = 'Restore password'
      redirect '/'
    end
  end
end
