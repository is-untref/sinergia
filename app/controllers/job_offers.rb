JobVacancy::App.controllers :job_offers do
  get :my do
    @offers = JobOffer.find_by_owner(current_user)
    render 'job_offers/my_offers'
  end

  get :index do
    @offers = JobOffer.all_active
    render 'job_offers/list'
  end

  get :new do
    @job_offer = JobOffer.new
    render 'job_offers/new'
  end

  get :latest do
    @offers = JobOffer.all_active
    flash.now[:error] = 'Only authenticated candidate can apply for the offers.' unless signed_in?
    render 'job_offers/list'
  end

  get :edit, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    # TODO: validate the current user is the owner of the offer
    render 'job_offers/edit'
  end

  get :apply, with: :offer_id do
    if signed_in?
      @job_offer = JobOffer.with_pk(params[:offer_id])
      @job_application = JobApplication.new
      # TODO: validate the current user is the owner of the offer
      render 'job_offers/apply'
    else
      redirect '/job_offers'
    end
  end

  get :job_applications, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    render 'job_offers/job_applications'
  end

  post :search do
    @offers = JobOffer.where(Sequel.ilike(:title, "%#{params[:q]}%"))
    flash.now[:error] = 'Only authenticated candidate can apply for the offers.' unless signed_in?
    render 'job_offers/list'
  end

  post :apply, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    applicant_email = params[:job_application][:applicant_email]
    @job_application = JobApplication.create_for(applicant_email, @job_offer)
    begin
      is_ok = @job_application.validate_email(applicant_email)
      raise StandardError, 'Invalid email format. Please try again.' unless is_ok
      raise StandardError, 'Please check the captcha' unless verify_recaptcha

      @job_application.values[:job_offer_id] = @job_offer.id
      @job_application.values[:applicant_email] = params[:job_application][:applicant_email]
      @job_application.values[:date_application] = Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
      @job_application.values[:user_id] = current_user.id
      @job_application.save
      @job_application.process
      send_mail(params, @job_offer)
      @suggested_offers = OfferSuggester.get_suggest(@job_offer)
      flash[:success] = 'Contact information sent.'
      redirect '/job_offers'
    rescue StandardError => e
      flash.now[:error] = e.message
      render 'job_offers/apply'
    end
  end

  post :create do
    @job_offer = JobOffer.new(params[:job_offer])
    @job_offer.owner = current_user
    if @job_offer.save
      TwitterClient.publish(@job_offer) if params['create_and_twit']
      flash[:success] = 'Offer created'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/new'
    end
  end

  post :update, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.update(params[:job_offer])
    if @job_offer.save
      flash[:success] = 'Offer updated'
      redirect '/job_offers/my'
    else
      flash.now[:error] = 'Title is mandatory'
      render 'job_offers/edit'
    end
  end

  put :activate, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.activate
    if @job_offer.save
      flash[:success] = 'Offer activated'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  put :desactivate, with: :offer_id do
    @job_offer = JobOffer.with_pk(params[:offer_id])
    @job_offer.deactivate
    if @job_offer.save
      flash[:success] = 'Offer satisfied'
    else
      flash.now[:error] = 'Operation failed'
    end

    redirect '/job_offers/my'
  end

  delete :destroy do
    offer_id = params[:offer_id]
    @job_offer = JobOffer.with_pk(offer_id)
    begin
      is_zero = get_count_job_applications(offer_id).zero?
      raise StandardError, 'You cannot delete an offer with applications' unless is_zero

      if @job_offer.destroy
        flash[:success] = 'Offer deleted'
      else
        flash.now[:error] = 'Title is mandatory'
      end
    rescue StandardError => e
      flash[:error] = e.message
    end

    redirect '/job_offers/my'
  end
end

private

def send_mail(params, job_offer)
  from = params[:job_application][:applicant_email]
  to = User.first(id: job_offer.user_id).email
  subject = job_offer.title
  question = params[:job_application][:offer_question]
  job_offer_question = JobOfferQuestion.new(from, to, subject, question)
  job_offer_question.send_mail
end
