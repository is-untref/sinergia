JobVacancy::App.mailer :restore_password do
  email :contact_info_email do |user|
    from 'JobVacancy@jobvacancy.com'
    to user.email
    subject 'Restore password Job Application'
    locals user: user
    content_type :plain
    render 'notification/restore_password_email'
  end
end
