Sequel.migration do
  up do
    add_column :users, :token, String
    add_column :users, :updated_on, DateTime
  end

  down do
    drop_column :users, :token
    drop_column :users, :updated_on
  end
end
