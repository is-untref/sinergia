Sequel.migration do
  up do
    alter_table(:job_applications) do
      set_column_type :date_application, DateTime
    end
  end

  down do
    drop_column :job_applications, :date_application
  end
end
