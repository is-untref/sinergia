Sequel.migration do
  up do
    create_table(:job_applications) do
      primary_key :id_application
      foreign_key :job_offer_id, :job_offers, null: false, on_delete: :cascade, on_update: :cascade
      String :applicant_email
      Date :date_application
    end
  end

  down do
    drop_table(:job_applications)
  end
end
