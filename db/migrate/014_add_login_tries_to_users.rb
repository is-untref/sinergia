Sequel.migration do
  up do
    add_column :users, :login_tries, Integer, default: 0
  end

  down do
    drop_column :users, :login_tries
  end
end
