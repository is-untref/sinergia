Sequel.migration do
  up do
    add_column :users, :block_date, DateTime
  end

  down do
    drop_column :users, :block_date
  end
end
