Sequel.migration do
  up do
    add_column :job_offers, :minimum_experience, Integer
  end

  down do
    drop_column :job_offers, :minimum_experience
  end
end
