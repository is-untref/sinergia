Sequel.migration do
  up do
    alter_table(:job_applications) do
      add_foreign_key :user_id, :users
    end
  end

  down do
    alter_table(:job_applications) do
      drop_foreign_key :user_id
    end
  end
end
