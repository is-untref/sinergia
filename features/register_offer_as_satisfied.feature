Feature: register an offer as satisfied
  As a offeror I want to be able to register an offer as satisfied and that it is not shown to the candidates

  Background:
  	Given an user cosme-fulanito logged in as job offerer

  Scenario: The offeror indicates that his offer is satisfied
  Given an existing offer already active with title "Desarrollador ruby"
  When the offeror indicates that this offer is satisfied
  Then the offer from the offerors list is shown as inactive
   And the following message is displayed "Offer satisfied"

  Scenario: Candidate can not have offers that have been indicated as satisfied	
  Given an existing offer already active with title "Desarrollador .NET y ASP"
  When the offeror indicates that this offer is satisfied
  Then the candidate has no visibility about offers satisfied "Desarrollador .NET y ASP" from his list

  Scenario: The offeror can not mark a satisfied offer when it is inactive
  Given an existing offer already inactive with title "Analista"
  Then offeror can not indicate it as satisfied because it does not have the option