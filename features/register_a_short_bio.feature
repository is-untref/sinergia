Feature: register a short bio
  As user I want to leave a personal description (short bio)

  Scenario: Successfully registered user with short bio
    Given a non-existent user with email "usuario@inexistente.com"
    When user fill the name with "Leonel"
     And he fill the password with "123456789!Mv"
     And he fill the confirmation password with "123456789!Mv"
     And he fill short bio as "Hola. Soy Leonel, estudiante de la facultad de 3 de Febrero-."
    And he confirms the creation
    Then user registered successfully

  Scenario: User cannot register because he entry blank his short bio
    Given a non-existent user with email "juan@inexistente.com"
    When user fill the name with "Juan"
     And he fill the password with "123456789!Mv"
     And he fill the confirmation password with "123456789!Mv"
     And he fill short bio as " "
    And he confirms the creation
    Then user can not register and the following message is displayed "All fields are mandatory"

  Scenario: Short bio is saved when user applies in an offer
    Given a user existent with email "futur@ing.com" and a description "Me estoy por recibir en la carrera de Ingeniería en Computación, me gusta jugar al ping pong y comer sandía"
      And an offer selected with title "Desarrollador Java"
    When the user completes the email in Applicant email as "futur@ing.com"
     And he applies
    Then his short bio is saved in the applied offer
