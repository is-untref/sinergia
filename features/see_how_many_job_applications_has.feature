Feature: As a offerer I want to know the number of applications of each of my offers

  Scenario: The offerer sees how many job applications he has in his offers
    Given a offer "Kioto develop" whit 3 job applications
    When the offeror access the my offers page
    Then in the offer "Kioto develop" 3 candidates were applied

    Given a offer "Tokio develop" whitout job applications
    When the offeror access the my offers page
    Then in the offer "Tokio develop" 0 candidates were applied
