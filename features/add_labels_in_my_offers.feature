Feature: as a offeror I want to add labels to my offers

  Scenario: create offer whit labels
    Given a new offert "Java Sr" and description "Java Sr, object oriented" in "Reutte"
    And the labels "java, Develop, object, oriented, SOFTWARE"
    When the offeror access the my offers page
    Then the offeror should see "java,develop,object,oriented,software" in My Offers

    Given a new offert "Ruby Sr" and description "Ruby Sr, object oriented" in "Milano"
    And the labels "Ruby, ruby On rails,  chef  "
    When the offeror access the my offers page
    Then the offeror should see "ruby,ruby on rails,chef" in My Offers
