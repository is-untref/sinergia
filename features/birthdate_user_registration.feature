Feature: Register Birthdate

  Background:
  	  Given I access to registration page
      And I fill user Name with "SinergiaBirth"
      And I fill Email with "Sinergia2018@test.com"
      And I fill Password with "Sinergia2018!"
      And I fill Password confirmation correctly
      And I fill Description with "It is a short sentence about me"

    Scenario: Create new usser
      Given I fill user Birthdate with "1900-01-01"
      When I press create button
      Then I should see "User created" on the homepage

    Scenario: Cant create new usser with future birthdate
      Given I fill user Birthdate with "2019-01-01"
      When I press create button  
      Then I should see "Birthdate cannot be in the future" on the registration page

    Scenario: Too young user cant create new account
      Given I fill user Birthdate with "2018-01-11"
      When I press create button  
      Then I should see "You must be at least 15 years of age" on the registration page

    Scenario: Invalid date format
      Given I fill user Birthdate with "yyyy-mm-dd"
      When I press create button  
      Then I should see "invalid date" on the registration page
