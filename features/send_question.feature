Feature: Send question
  As an applicant I want to send question about offers

  Background:
    Given An existing offer with the title "Python & Django" and owner is "user@offerer.com"

  Scenario: Send a mail with a question
    Given I fill the mail "user@applicant.com"
      And I fill the question with "Do they work with agile methodologies?"
      And I fill the captcha
    When I apply about the offer
    Then The offerer should receive a mail with the question and as a subject the title of the offer

  Scenario: No send a mail with empty question
    Given I fill the mail "another_user@applicant.com"
      And I fill the question with " "
      And I fill the captcha
    When I apply about the offer
    Then The offerer not should receive a mail with the question
