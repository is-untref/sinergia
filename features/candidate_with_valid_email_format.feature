Feature: Candidate with valid email format for apply to offer

	Scenario: Candidate with valid email format send your email to receive the contanct information for this offer
		Given an authenticated candidate user
		And an active job offer "Java Sr develop"
		When the candidate applies with email "valid@email.ok"
		Then The following message is displayed "Contact information sent"

	Scenario: Candidate with invalid email format send your email to receive the contanct information for this offer
		Given an authenticated candidate user
		And an active job offer "Ruby fail"
		When the candidate applies with email "xyz"
		Then The following message is displayed "Invalid email format"
