Given(/^an existing user with the email "(.*?)" and password "(.*?)"$/) do |email, password|
  @user = User.first(email: email)
  @user&.destroy unless @user.nil?
  @user = User.create(email: email, name: 'Cosme',
                      password: password, short_bio: 'Hi. I am Cosme')
  @e_mail = email
  @a_password = password
end

When(/^user entries to login$/) do
  visit '/login'
end

When(/^he fill the mail "(.*?)"$/) do |email|
  fill_in('user[email]', with: email)
end

When(/^he re-enters the wrong password "(.*?)" "(.*?)" times$/) do |wrong_password, time|
  time.to_i.times do
    fill_in('user[email]', with: @e_mail)
    fill_in('user[password]', with: wrong_password)
    click_button('Login')
  end
end

When(/^he enters the wrong password "(.*?)" filling the captcha "(.*?)" times more consecutively$/) do |wrong_password, time|
  time.to_i.times do
    fill_in('user[email]', with: @e_mail)
    fill_in('user[password]', with: wrong_password)
    click_button('Login')
  end
end

Then(/^user blocked for 24 hours$/) do
  actual = User.first(email: @e_mail).block_date
  expect(actual).not_to be_nil
end

Then(/^the message "(.*?)" is displayed$/) do |message|
  page.should have_content(message)
end

Given(/^he is blocked "(.*?)" hours ago$/) do |hours|
  @actual = User.first(email: @e_mail)
  @actual.block_date = Time.zone.now - (hours.to_i * 60 * 60)
  @actual.save
end

When(/^he fill the password with correct value "(.*?)"$/) do |a_password|
  fill_in('user[email]', with: @e_mail)
  fill_in('user[password]', with: a_password)
end

When(/^he confirms to login$/) do
  click_button('Login')
end

Then(/^user logged in as "(.*?)"$/) do |email|
  page.should have_content(email)
end
