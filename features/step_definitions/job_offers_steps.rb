When(/^I browse the default page$/) do
  visit '/'
end

Given(/^I am logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^I access the new offer page$/) do
  visit '/job_offers/new'
  page.should have_content('Title')
end

When(/^I fill the title with "(.*?)"$/) do |offer_title|
  fill_in('job_offer[title]', with: offer_title)
end

When(/^confirm the new offer$/) do
  click_button('Create')
end

Then(/^I should see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should have_content(content)
end

Then(/^I should not see "(.*?)" in My Offers$/) do |content|
  visit '/job_offers/my'
  page.should_not have_content(content)
end

Given(/^I have "(.*?)" offer in My Offers$/) do |offer_title|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: offer_title)
  click_button('Create')
end

Given(/^I edit it$/) do
  click_link('Edit')
end

And(/^I delete it$/) do
  click_button('Delete')
end

Given(/^I set title to "(.*?)"$/) do |new_title|
  fill_in('job_offer[title]', with: new_title)
end

Given(/^I save the modification$/) do
  click_button('Save')
end

Given(/^The offer "(.*?)" has 1 applications$/) do |title_offer|
  offer = JobOffer.first(title: title_offer)
  time_application = Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
  job_application = JobApplication.create_for('cosme@test.com', offer)
  user_cosme = User.create(email: 'cosme@test.com',
                           name: 'Cosme',
                           password: '123456789!Mv',
                           short_bio: 'I am Cosme')
  job_application.values[:job_offer_id] = offer.id
  job_application.values[:applicant_email] = 'cosme@test.com'
  job_application.values[:date_application] = time_application
  job_application.values[:user_id] = user_cosme.id
  job_application.save
end

When(/^I try to delete it$/) do
  click_button('Delete')
end
