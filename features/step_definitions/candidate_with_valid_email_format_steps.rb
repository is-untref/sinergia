When(/^the candidate applies with email "(.*?)"$/) do |email|
  visit '/job_offers'
  first(:xpath, "//tr[contains(.,'spa develop')]/td/a", text: 'Apply').click
  fill_in('job_application[applicant_email]', with: email)
  page.should have_selector('.g-recaptcha')
  click_button('Apply')
end

Then(/^The following message is displayed "(.*?)"$/) do |message|
  page.should have_content(message)
end
