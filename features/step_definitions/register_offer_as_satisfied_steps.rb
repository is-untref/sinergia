Given(/^an user cosme-fulanito logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^an existing offer already active with title "(.*?)"$/) do |a_title|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: a_title)
  click_button('Create')
end

When(/^the offeror indicates that this offer is satisfied$/) do
  click_button('Satisfied')
end

Then(/^the offer from the offerors list is shown as inactive$/) do
  JobOffer.all.each do |item|
    expect(item.is_active).to be false
  end
end

And(/^the following message is displayed "(.*?)"$/) do |a_message|
  page.should have_content(a_message)
end

Then(/^the candidate has no visibility about offers satisfied "(.*?)" from his list$/) do |a_title|
  visit '/job_offers/latest'
  page.should have_no_content(a_title)
end

Given(/^an existing offer already inactive with title "(.*?)"$/) do |a_title|
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  fill_in('job_offer[title]', with: a_title)
  click_button('Create')
  click_button('Satisfied')
end

Then(/^offeror can not indicate it as satisfied because it does not have the option$/) do
  visit '/job_offers/my'
  page.should have_no_content('Satisfied')
end
