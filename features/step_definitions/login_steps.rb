Given('an user with mail sinergiaLogin@gmail.com and password Sinergia2018! registered in the application') do
  @user = User.first(email: 'sinergiaLogin@gmail.com')
  @user&.destroy unless @user.nil?
  @user = User.create(email: 'sinergiaLogin@gmail.com',
                      name: 'sinergiaLogin',
                      password: 'Sinergia2018!',
                      short_bio: 'Hi. We are Sinergia.')
end

Given('the user its login page') do
  visit '/login'
end

Given('the email field its always filled correctly') do
  fill_in('user[email]', with: 'sinergiaLogin@gmail.com')
end

Given('I fill the password with {string} value on login form') do |password|
  fill_in('user[password]', with: password)
end

Given('I already fill wrong the password {int} times') do |times|
  times.times do
    fill_in('user[password]', with: 'wrongPasswordValue')
    click_button('Login')
    fill_in('user[email]', with: 'sinergiaLogin@gmail.com')
  end
end

Given('I log out') do
  visit '/logout'
end

When('I press login button') do
  click_button('Login')
end

When('the user goes to login page') do
  visit '/login'
end

Then('I should be logged in the application') do
  page.should have_content('sinergiaLogin@gmail.com')
end

Then('I should not see any captcha') do
  page.should have_no_selector('.g-recaptcha')
end

Then('I should see a captcha') do
  page.should have_selector('.g-recaptcha')
end
