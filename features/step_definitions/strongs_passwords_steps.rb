Given('I fill the password with {string} value on new user form') do |sended_password|
  visit '/register'
  @password = sended_password
  fill_in('user[password]', with: @password)
end

When('I create a new user') do
  fill_in('user[name]', with: 'user_test')
  fill_in('user[email]', with: 'user@test.com')
  fill_in('user[password_confirmation]', with: @password)
  fill_in('user[short_bio]', with: 'Hi. I am user_test')
  fill_in('user[birthdate]', with: '1990-01-01')
  click_button('Create')
end

Then('I should see {string} on the homepage') do |homepage_messagge|
  page.should have_content(homepage_messagge)
end

Then('I should see {string} on the registration page') do |registration_page_message|
  page.should have_content(registration_page_message)
end
