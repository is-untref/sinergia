Given(/^An existing offer with the title "(.*?)" and owner is "(.*?)"$/) do |title, mail|
  user = create_user(mail, 'Offerer')
  @job_offer = JobOffer.new
  @job_offer.owner = user
  @job_offer.title = title
  @job_offer.save
  @title = title
  @mail_owner = mail
end

Given(/^I fill the mail "(.*?)"$/) do |mail|
  user = create_user(mail, 'An applicant')
  visit '/login'
  fill_in('user[email]', with: user.email)
  fill_in('user[password]', with: 'Sinergia2018!')
  click_button('Login')
  visit '/job_offers'
  find(:xpath, "//tr[contains(.,'" + @title + "')]/td/a", match: :first, text: 'Apply').click
  fill_in('job_application[applicant_email]', with: user.email)
  @mail_from = user.email
end

Given(/^I fill the question with "(.*?)"$/) do |question|
  fill_in('job_application[offer_question]', with: question)
  @question = question
end

Given(/^I fill the captcha$/) do
  page.should have_selector('.g-recaptcha')
end

When(/^I apply about the offer$/) do
  click_button('Apply')
end

Then(/^The offerer should receive a mail with the question and as a subject the title of the offer$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/#{@mail_owner}", 'r')
  content = file.read
  content.include?(@question).should be true
end

Then(/^The offerer not should receive a mail with the question$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/#{@mail_owner}", 'r')
  content = file.read
  content.include?(@mail_from).should be false
end

private

def create_user(a_mail, a_name)
  destroy_user(a_mail)
  user = User.create(email: a_mail, name: a_name,
                     password: 'Sinergia2018!',
                     short_bio: 'Hi')
  user
end

def destroy_user(a_mail)
  user = User.first(email: a_mail)
  user&.destroy unless user.nil?
end
