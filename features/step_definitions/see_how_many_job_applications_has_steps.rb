Given('a offer {string} whit {int} job applications') do |title_offer, count_application|
  a_user = User.first(email: 'offerer@test.com')
  @a_job_offert = JobOffer.new
  @a_job_offert.owner = a_user
  @a_job_offert.title = title_offer
  @a_job_offert.location = 'Kioto'
  @a_job_offert.description = 'a develop in Kioto'
  @a_job_offert.save
  (1..count_application).each do |iterator|
    time_application = Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
    a_email = 'test' + iterator.to_s + '@test.com'
    @job_application = JobApplication.create_for(a_email, @a_job_offert)
    other_user = User.last
    @job_application.values[:job_offer_id] = @a_job_offert.id
    @job_application.values[:applicant_email] = a_email
    @job_application.values[:date_application] = time_application
    @job_application.values[:user_id] = other_user.id
    @job_application.save
  end
end

Given('a offer {string} whitout job applications') do |title_offer|
  a_user = User.first(email: 'offerer@test.com')
  @a_job_offert = JobOffer.new
  @a_job_offert.owner = a_user
  @a_job_offert.title = title_offer
  @a_job_offert.location = 'Tokio'
  @a_job_offert.description = 'a develop in Tokio'
  @a_job_offert.save
end

When('the offeror access the my offers page') do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  visit '/job_offers/my'
end

Then('in the offer {string} {int} candidates were applied') do |title_offer, count_application|
  page.should have_content(count_application)
  td = page.find(:css, 'td.title_offer', text: title_offer) # find the id td with text of exactly 22
  tr = td.find(:xpath, './parent::tr') # get the parent tr of the td
  expect(tr).to have_css('td.text-right', text: count_application)
end
