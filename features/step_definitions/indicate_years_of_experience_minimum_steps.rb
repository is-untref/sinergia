Given(/^a new publication containing the title "(.*?)", a location with value "(.*?)" and a description with value "(.*?)"$/) do |a_title, a_location, a_description|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = a_title
  @job_offer.location = a_location
  @job_offer.description = a_description
  @description = @job_offer.description
end

When(/^the offeror enters a minimum amount of experience with a value of "(.*?)" years$/) do |a_minimum_experience|
  @job_offer.minimum_experience = a_minimum_experience
  @job_offer.save
end

Then(/^the offer appears with the value "(.*?)" in the minimum experience required$/) do |a_minimum_experience|
  actual = a_minimum_experience.to_s
  expected = @job_offer.minimum_experience.to_s
  expect(actual).to eq(expected)
end
