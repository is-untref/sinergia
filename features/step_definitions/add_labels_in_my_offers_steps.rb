Given(/^a new offert "(.*?)" and description "(.*?)" in "(.*?)"$/) do |title, description, location|
  @job_offert = JobOffer.new
  @job_offert.owner = User.first(email: 'offerer@test.com')
  @job_offert.title = title
  @job_offert.location = location
  @job_offert.description = description
end

Given(/^the labels "(.*?)"$/) do |a_labels|
  @job_offert.labels = a_labels
  @job_offert.save
end

Then(/^the offeror should see "(.*?)" in My Offers$/) do |a_labels|
  page.should have_content(a_labels)
end
