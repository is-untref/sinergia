Given('I access to registration page') do
  visit '/register'
end

Given('I fill user Name with {string}') do |name_user|
  fill_in('user[name]', with: name_user)
end

Given('I fill Email with {string}') do |email|
  fill_in('user[email]', with: email)
end

Given('I fill Password with {string}') do |password|
  @password = password
  fill_in('user[password]', with: password)
end

Given('I fill Password confirmation correctly') do
  fill_in('user[password_confirmation]', with: @password)
end

Given('I fill Description with {string}') do |short_bio|
  fill_in('user[short_bio]', with: short_bio)
end

Given('I fill user Birthdate with {string}') do |birthdate|
  fill_in('user[birthdate]', with: birthdate)
end

When('I press create button') do
  click_button('Create')
end
