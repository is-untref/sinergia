Given(/^only a "(.*?)" offer exists in the offers list$/) do |job_title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = job_title
  @job_offer.location = 'a nice job'
  @job_offer.description = 'a nice job'
  @job_offer.save
end

Given(/^I access the offers list page$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  visit '/job_offers'
end

Given(/^an authenticated candidate user$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  visit '/job_offers'
end

Given(/^an active job offer "(.*?)"$/) do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = 'spa develop'
  @job_offer.description = 'spa develop'
  @job_offer.save
end

Given(/^an not authenticated candidate user$/) do
  visit '/job_offers'
end

When(/^I apply$/) do
  find(:xpath, "//tr[contains(.,'a nice job')]/td/a", text: 'Apply').click
  fill_in('job_application[applicant_email]', with: 'applicant@test.com')
  click_button('Apply')
end

When(/^viewing the list of offers$/) do
  visit '/job_offers'
end

Then(/^I should receive a mail with offerer info$/) do
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/applicant@test.com", 'r')
  content = file.read
  content.include?(@job_offer.title).should be true
  content.include?(@job_offer.location).should be true
  content.include?(@job_offer.description).should be true
  content.include?(@job_offer.owner.email).should be true
  content.include?(@job_offer.owner.name).should be true
end

Then(/^he can apply for the "(.*?)" offer$/) do |title|
  find(:xpath, "//tr[contains(.,'" + title + "')]/td/a", text: 'Apply').click
  fill_in('job_application[applicant_email]', with: 'applicant@test.com')
  click_button('Apply')
end

Then(/^he can not apply for the "(.*?)" offer$/) do |title|
  page.should have_content(title)
  page.should_not have_button('Apply', disabled: true)
end
