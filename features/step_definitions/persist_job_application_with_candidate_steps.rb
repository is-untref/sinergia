Given(/^active job offer "(.*?)" with owner "(.*?)"$/) do |a_title, a_owner|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first(email: a_owner)
  @job_offer.title = a_title
  @job_offer.location = 'a spa develop'
  @job_offer.description = 'a spa develop'
  @job_offer.save
end

Then(/^the job offer is stored in the system with email "(.*?)" and current date$/) do |a_email|
  @application = JobApplication.last(applicant_email: a_email)
  a_job_offert = JobOffer.first(id: @application.job_offer_id)
  expect(a_job_offert.title).to eq(@job_offer.title)
  expect(a_job_offert.location).to eq(@job_offer.location)
  current = Time.zone.now.strftime('%Y-%m-%d')
  expect(@application.date_application.strftime('%Y-%m-%d')).to eq(current)
end
