Given(/^a user "(.*?)" who forgot his password$/) do |a_email|
  @email = a_email
  @user = User.first(email: a_email)
  @user&.destroy unless @user.nil?
  @user = User.create(email: a_email, name: 'Matias',
                      password: 'Sinergia2018!', short_bio: 'I am develop')
  visit '/forgot_password'
  fill_in('user[email]', with: a_email)
end

Given(/^a invalid user "(.*?)" who forgot his password$/) do |a_email|
  @email = a_email
  @user = User.first(email: a_email)
  visit '/forgot_password'
  fill_in('user[email]', with: a_email)
end

When(/^request that the token be sent$/) do
  click_button('Send')
end

Then(/^the user should receive a mail with link to restore password$/) do
  user = User.first(email: @email)
  mail_store = "#{Padrino.root}/tmp/emails"
  file = File.open("#{mail_store}/" + @email, 'r')
  content = file.read
  content.include?(user.name).should be true
  content.include?(user.token).should be true
end

Then(/^the message "(.*?)" should be displayed$/) do |message|
  page.should have_content(message)
end

Given(/^a user "(.*?)"$/) do |a_email|
  @user = User.first(email: a_email)
end

Given('the time of {int} minutes of token not expired') do |time_limit|
  @user.updated_on = time_limit.minutes.ago
end

Given(/^the token "(.*?)"$/) do |a_token|
  @a_token = a_token
  @user.token = a_token
  @user.save
end

When('the user go to restore password') do
  visit '/users/edit/' + @a_token
end

When(/^the user fill the password with "(.*?)"$/) do |a_password|
  fill_in('user[password]', with: a_password)
end

When(/^the user fill the password confirmation with "(.*?)"$/) do |a_password_confirmation|
  fill_in('user[password_confirmation]', with: a_password_confirmation)
end

When(/^restore password$/) do
  click_button('Restore')
  @user.token = nil
end

Then(/^the new password is "(.*?)"$/) do |a_password|
  expect(@user.has_password?(a_password)).to be_truthy
end

Then(/^the message "(.*?)" should be displayed because you can use the token only one time if change your password$/) do |message|
  page.should have_content(message)
  expect(@user.token.nil?).to be_truthy
end

Given('the time of {int} minutes of token expired') do |time_limit|
  @user.updated_on = time_limit.minutes.ago
end
