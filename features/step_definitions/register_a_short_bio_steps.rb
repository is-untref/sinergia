Given(/^a non-existent user with email "(.*?)"$/) do |email|
  visit '/register'
  fill_in('user[email]', with: email)
  fill_in('user[birthdate]', with: '1990-01-01')
end

When(/^user fill the name with "(.*?)"$/) do |name|
  fill_in('user[name]', with: name)
end

When(/^he fill the password with "(.*?)"$/) do |password|
  fill_in('user[password]', with: password)
end

When(/^he fill the confirmation password with "(.*?)"$/) do |confirmation_password|
  fill_in('user[password_confirmation]', with: confirmation_password)
end

When(/^he fill short bio as "(.*?)"$/) do |short_bio|
  fill_in('user[short_bio]', with: short_bio)
end

When(/^he confirms the creation$/) do
  click_button('Create')
end

Then(/^user registered successfully$/) do
  page.should have_content('User created')
end

@title = nil
Given(/^a user existent with email "(.*?)" and a description "(.*?)"$/) do |email, short_bio|
  User.create(email: email,
              name: 'Sinergia',
              password: '123456789!Mv',
              short_bio: short_bio,
              birthdate: Date.new(1900, 1, 1))
  visit '/login'
  fill_in('user[email]', with: email)
  fill_in('user[password]', with: '123456789!Mv')
  click_button('Login')
  page.should have_content(email)
  @an_email = email
end

Given(/^an offer selected with title "(.*?)"$/) do |title|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first(email: 'offerer@test.com')
  @job_offer.title = title
  @job_offer.save
  @title = title
end

When(/^the user completes the email in Applicant email as "(.*?)"$/) do |applicant|
  visit '/job_offers'
  find(:xpath, "//tr[contains(.,'" + @title + "')]/td/a", text: 'Apply').click
  fill_in('job_application[applicant_email]', with: applicant)
end

When(/^he applies$/) do
  click_button('Apply')
end

Then(/^his short bio is saved in the applied offer$/) do
  actual = User.first(name: 'Sinergia').short_bio.include? 'gusta jugar al ping pong'
  expect(actual).to be_truthy
end

Then(/^user can not register and the following message is displayed "(.*?)"$/) do |message|
  page.should have_content(message)
end
