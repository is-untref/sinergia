Given(/^an offer of "(.*?)" with description "(.*?)" located in "(.*?)"$/) do |title, description, location|
  @job_offer = JobOffer.new
  @job_offer.owner = User.first
  @job_offer.title = title
  @job_offer.location = location
  @job_offer.description = description
  @job_offer.save
end

When(/^candidate search "(.*?)"$/) do |word|
  visit '/job_offers/latest'
  fill_in('q', with: word)
  click_button('search-button')
end

Then(/^the offer with the title "(.*?)" should be displayed$/) do |title|
  page.should have_content(title)
end
