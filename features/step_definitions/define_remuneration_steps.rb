@remuneration = nil

Given(/^offeror logged in as job offerer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  page.should have_content('offerer@test.com')
end

Given(/^offeror accesses a new offer$/) do
  JobOffer.all.each(&:delete)
  visit '/job_offers/new'
  page.should have_content('Title')
end

Given(/^he completes the title with "(.*?)" and description "(.*?)"$/) do |a_title, a_description|
  fill_in('job_offer[title]', with: a_title)
  fill_in('job_offer[description]', with: a_description)
end

Given(/^add as a value "(.*?)" in the remuneration for the position$/) do |a_remuneration|
  fill_in('job_offer[remuneration]', with: a_remuneration)
  @remuneration = a_remuneration
end

Given(/^he does not complete the remuneration for the position$/) do
  fill_in('job_offer[remuneration]', with: nil)
end

When(/^he confirms the new offer$/) do
  click_button('Create')
end

Then(/^the offer is created successfully with remuneration$/) do
  visit '/job_offers/latest'
  page.should have_content(@remuneration)
end

Then(/^the offer is not created$/) do
  visit '/job_offers/latest'
end

Then(/^the offer is created successfully without remuneration$/) do
  visit '/job_offers/latest'
  page.should have_content('not specified')
end
