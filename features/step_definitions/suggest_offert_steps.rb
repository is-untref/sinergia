Given(/^existing 2 offers with label ruby$/) do
  user = User.first(email: 'suggest@test.com')
  user&.destroy unless user.nil?
  user = User.create(email: 'suggest@test.com', name: 'SuggesterOne',
                     password: 'Sinergia2018!', short_bio: 'Im the suggester')
  job_offer = JobOffer.new
  job_offer.owner = user
  job_offer.title = 'an offer with label ruby'
  job_offer.labels = 'ruby'
  job_offer.save
  job_offer_two = JobOffer.new
  job_offer_two.owner = user
  job_offer_two.title = 'another offer with label ruby'
  job_offer_two.labels = 'ruby'
  job_offer_two.save
end

Given(/^There is an offer with label "(.*?)"$/) do |label|
  @job_application = JobApplication.first(applicant_email: 'apply_suggest@test.com')
  @job_application&.destroy unless @job_application.nil?
  user = User.first(email: 'apply_suggest@test.com')
  user&.destroy unless user.nil?
  user = User.create(email: 'apply_suggest@test.com', name: 'ApplySuggester',
                     password: 'Sinergia2018!', short_bio: 'Im the apply suggest')
  @a_job_offer = JobOffer.new
  @a_job_offer.owner = user
  @a_job_offer.title = 'Ruby' + label + 'offer'
  @a_job_offer.labels = label
  @a_job_offer.save
end

When(/^I apply to this offer$/) do
  visit '/login'
  fill_in('user[email]', with: 'apply_suggest@test.com')
  fill_in('user[password]', with: 'Sinergia2018!')
  click_button('Login')

  visit '/job_offers'
  find(:xpath, "//tr[contains(.,'" + @a_job_offer.title + "')]/td/a", text: 'Apply').click
  fill_in('job_application[applicant_email]', with: 'apply_suggest@test.com')
  click_button('Apply')
end

Then(/^I should see the message "(.*?)" and the other 2 offers$/) do |message|
  page.should have_content(message)
  page.should have_content('an offer with label ruby')
  page.should have_content('another offer with label ruby')
end

Then(/^I should not see the message "(.*?)"$/) do |message|
  page.should_not have_content(message)
end
