Given(/^an existing offer$/) do
  a_user = User.first(email: 'offerer@test.com')
  @a_job_offert = JobOffer.first(user_id: a_user.id)
end

Given(/^a new offer without associated applications$/) do
  a_user = User.first(email: 'offerer@test.com')
  @a_job_offert = JobOffer.new
  @a_job_offert.owner = a_user
  @a_job_offert.title = 'test develop'
  @a_job_offert.location = 'Neuquen'
  @a_job_offert.description = 'a develop'
  @a_job_offert.save
end

Given(/^an application of the candidate with "(.*?)" and a description "(.*?)" today$/) do |email, description|
  @time_application = Time.zone.now.strftime('%Y-%m-%d %H:%M:%S')
  @job_application = JobApplication.create_for(email, @a_job_offert)
  other_user = User.create(email: email,
                           name: 'valid',
                           password: 'Sinergia2018!',
                           short_bio: description)
  @job_application.values[:job_offer_id] = @a_job_offert.id
  @job_application.values[:applicant_email] = email
  @job_application.values[:date_application] = @time_application
  @job_application.values[:user_id] = other_user.id
  @job_application.save
end

When(/^offerer goes to the applications of the offer$/) do
  visit '/login'
  fill_in('user[email]', with: 'offerer@test.com')
  fill_in('user[password]', with: 'Passw0rd!')
  click_button('Login')
  visit '/job_offers/my'
  find(:xpath, "//tr[contains(.,'" + @a_job_offert.title + "')]/td/a",
       text: @a_job_offert.title).click
end

Then(/^the offerer should see "(.*?)"$/) do |description|
  page.should have_content(description)
end

Then(/^the offerer should see date today$/) do
  date = DateTime.parse(@time_application)
  page.should have_content(date.strftime('%m/%d/%Y'))
  page.should have_content(date.strftime('%I:%M %P'))
end
