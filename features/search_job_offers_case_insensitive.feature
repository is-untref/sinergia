Feature: search case insesitive
  As a candidate,  I want to search offers in case insensitive so that I could find all the possible results

  Scenario: candidate search title of the publication in uppercase
    Given an offer of "JAVA Developer" with description "Wanted developer java jr" located in "San Miguel"
    When candidate search "DEVELOPER"
    Then the offer with the title "JAVA Developer" should be displayed

  Scenario: candidate search title of the publication in uppercase and lowercase
    Given an offer of "JAVA Developer" with description "Wanted developer java jr" located in "San Miguel"
    When candidate search "DEVELoper"
    Then the offer with the title "JAVA Developer" should be displayed

  Scenario: candidate search a publication in lowercase
    Given an offer of "JAVA Developer" with description "Wanted developer java jr" located in "San Miguel"
    When candidate search "java"
    Then the offer with the title "JAVA Developer" should be displayed

    Scenario: candidate search a publication in lowercase with many publications
    Given an offer of "JAVA Developer" with description "Wanted developer java jr" located in "San Miguel"
    And an offer of "Ruby DEVELOPER" with description "Wanted developer ruby sr" located in "Bella vista"
    And an offer of "Server administrator" with description "Wanted server administrator linux" located in "Muñiz"
    When candidate search "developer"
    Then the offer with the title "JAVA Developer" should be displayed
    And the offer with the title "Ruby DEVELOPER" should be displayed
