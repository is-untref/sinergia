Feature: Apllication Login
  
  Background: 
    Given an user with mail sinergiaLogin@gmail.com and password Sinergia2018! registered in the application
	And the user its login page
	And the email field its always filled correctly
  
  Scenario: correct login
    Given I fill the password with "Sinergia2018!" value on login form
    When I press login button    
    Then I should be logged in the application

  Scenario: correct login with one wrong try
    Given I already fill wrong the password 1 times
	And I fill the password with "Sinergia2018!" value on login form
	When I press login button
	Then I should not see any captcha

  Scenario: correct login with two wrong tries
    Given I already fill wrong the password 2 times
	And I fill the password with "Sinergia2018!" value on login form
	When I press login button
	Then I should not see any captcha

  Scenario: correct login with three wrong tries
    Given I already fill wrong the password 4 times
	When I press login button
	Then I should see a captcha

  Scenario: correct login after the captcha
    Given I already fill wrong the password 3 times
    And the email field its always filled correctly
	And I fill the password with "Sinergia2018!" value on login form
	And I press login button
	And I log out
	When the user goes to login page
	Then I should not see any captcha
