Feature: Suggest offer
As candidate
I want to see suggested offers

  Background:
  	Given existing 2 offers with label ruby

  Scenario: show offers
    Given There is an offer with label "ruby ssr"
    When I apply to this offer
    Then I should see the message "These are some positions that migth also interes you" and the other 2 offers

  Scenario: not show offers
    Given There is an offer with label "java"
    When I apply to this offer
    Then I should not see the message "These are some positions that migth also interes you"
