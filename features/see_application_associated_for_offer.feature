Feature: As a offerer I want to see the applications associated with each offer (date, application time, email and shortbio)

  Scenario: offer with associated applications

    Given an existing offer
    And an application of the candidate with "valid@email.com" and a description "I want to work" today
    When offerer goes to the applications of the offer
    Then the offerer should see "valid@email.com"
    And the offerer should see "I want to work"
    And the offerer should see date today

  Scenario: offer without associated applications

    Given a new offer without associated applications
    When offerer goes to the applications of the offer
    Then the offerer should see "The offer has no applications yet"
