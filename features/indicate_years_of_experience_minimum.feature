Feature: indicate years of experience required for an offer
  As a offeror I want to indicate the years of experience required so that apply

Scenario: The offeror creates a publication indicating the years of experience the offeree should have
  Given a new publication containing the title "Desarrollador Ruby", a location with value "Devoto" and a description with value "Desarrollador con amplio conocimiento en ruby y padrino"
  When the offeror enters a minimum amount of experience with a value of "3" years
  Then the offer appears with the value "3" in the minimum experience required
