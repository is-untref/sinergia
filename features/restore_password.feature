Feature: Restore password

  Scenario: send email with token
    Given a user "matias@test.com" who forgot his password
     When request that the token be sent
     Then the user should receive a mail with link to restore password
      And the message "Link for restore password sent by mail" should be displayed

  Scenario: restore password
    Given a user "matias@test.com" 
      And the time of 10 minutes of token not expired
      And the token "k0wcch7q"
     When the user go to restore password
      And the user fill the password with "Sinergia2018!"
      And the user fill the password confirmation with "Sinergia2018!"
      And restore password
     Then the new password is "Sinergia2018!"
      And the message "Restore password" should be displayed

  Scenario: cannot restore password
    Given a user "matias@test.com" 
      And the time of 11 minutes of token expired
      And the token "k0wcch7q"
     When the user go to restore password
     Then the message "The time for restore password has expired." should be displayed

  Scenario: cannot re-restore password with used token
    Given a user "matias@test.com" 
      And the time of 7 minutes of token expired
      And the token "k0wcch7q"
     When the user go to restore password
      And the user fill the password with "Sinergia2018!"
      And the user fill the password confirmation with "Sinergia2018!"
      And restore password
      And the user go to restore password
     Then the message "The link to restore password is invalid, request again from the login page" should be displayed because you can use the token only one time if change your password

  Scenario: cannot send email with token for invalid user
    Given a invalid user "invalid-to-restore@user.com" who forgot his password
     When request that the token be sent
     Then the message "Invalid user" should be displayed
