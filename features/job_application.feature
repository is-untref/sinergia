Feature: Job Application
  In order to get a job
  As a candidate
  I want to apply to an offer

  Background:
    Given only a "Web Programmer" offer exists in the offers list

  Scenario: Apply to job offer
    Given I access the offers list page
    When I apply
    Then I should receive a mail with offerer info

  Scenario: Registered user applies in an offer
    Given an authenticated candidate user
    And an active job offer "Java Ssr"
    When viewing the list of offers
    Then he can apply for the "Java Ssr" offer

  Scenario: Unregistered user applies in an offer
    Given an not authenticated candidate user
    And an active job offer "Java Ssr"
    When viewing the list of offers
    Then he can not apply for the "Java Ssr" offer
