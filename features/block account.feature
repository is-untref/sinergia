Feature: Block account
  As a user I want my account to be blocked before 6 unsuccessful login attempts

Scenario: User blocked by 6 failed password entry attempts
  Given an existing user with the email "usuario@bloqueado.com" and password "123456789!Mv"
  When user entries to login
   And he fill the mail "usuario@bloqueado.com"
   And he re-enters the wrong password "123456789" "3" times
   And he enters the wrong password "123456789" filling the captcha "4" times more consecutively
  Then user blocked for 24 hours
   And the message "Your account has been blocked for 24 hours" is displayed

Scenario: The user is unlocked when the 24 hours have passed blocking
  Given an existing user with the email "usuaria@bloqueada.com" and password "123456789!Mv"
    And he is blocked "26" hours ago
  When user entries to login
   And he fill the mail "usuaria@bloqueada.com"
   And he fill the password with correct value "123456789!Mv"
   And he confirms to login
  Then user logged in as "usuaria@bloqueada.com"