Feature: define the remuneration of the position
  As a offeror I want to define the remuneration of the position

Background:
  Given offeror logged in as job offerer

Scenario: Offeror creates an offer with a remuneration for the position
  Given offeror accesses a new offer
    And he completes the title with "Programador web" and description "Full Time"
    And add as a value "25000" in the remuneration for the position
  When he confirms the new offer
  Then the offer is created successfully with remuneration

Scenario: the offer is not created when the remuneration is greater than 1000000
  Given offeror accesses a new offer
    And he completes the title with "Programador web" and description "Full Time"
    And add as a value "9999999" in the remuneration for the position
  When he confirms the new offer
  Then the offer is not created

Scenario: offeror creates an offer but without remuneration for the position
  Given offeror accesses a new offer
    And he completes the title with "Programador web" and description "Full Time"
    And he does not complete the remuneration for the position
  When he confirms the new offer
  Then the offer is created successfully without remuneration
