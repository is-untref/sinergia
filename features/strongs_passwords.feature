Feature: strongs passwords

  Scenario: accepted password
    Given I fill the password with "Sinergia**18" value on new user form
    When I create a new user    
    Then I should see "User created" on the homepage
    

  Scenario:  password rejected for minimum length
    Given I fill the password with "Sinergia18" value on new user form
    When I create a new user    
    Then I should see "The password must be at least 12 characters" on the registration page

  Scenario:  password rejected for not containing upper case
    Given I fill the password with "sinergia**18" value on new user form
    When I create a new user    
    Then I should see "The password must constains at least one upper case character" on the registration page

  Scenario:  password rejected for not containing lower case
    Given I fill the password with "SINERGIA**18" value on new user form
    When I create a new user    
    Then I should see "The password must constains at least one lower case character" on the registration page

  Scenario:  password rejected for not containing numbers
    Given I fill the password with "Sinergia****" value on new user form
    When I create a new user    
    Then I should see "The password must constains at least one number character" on the registration page

  Scenario:  password rejected for not containing symbols
    Given I fill the password with "Sinergia2018" value on new user form
    When I create a new user 
    Then I should see "The password must constains at least one symbol" on the registration page