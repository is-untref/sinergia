Feature: As a offerer, I want you to collect information about the applications and that this information is stored in the system

Scenario: stored the job offer with a candidate
    Given an authenticated candidate user
    And active job offer "Java Ssr" with owner "offerer@test.com"
    When the candidate applies with email "valid@email.ok"
    Then the job offer is stored in the system with email "valid@email.ok" and current date