require 'spec_helper'
require 'date'

describe User do
  subject(:user) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:crypted_password) }
    it { is_expected.to respond_to(:email) }
    it { is_expected.to respond_to(:job_offers) }
    it { is_expected.to respond_to(:short_bio) }
    it { is_expected.to respond_to(:login_tries) }
    it { is_expected.to respond_to(:block_date) }
  end

  describe 'valid?' do
    it 'should be false when name is blank' do
      user.email = 'john.doe@someplace.com'
      user.password = 'a_secure_passWord!'
      expect(user.valid?).to eq false
    end

    it 'should be false when email is not valid' do
      user.name = 'John Doe'
      user.email = 'john'
      user.password = 'a_secure_passWord!'
      expect(user.valid?).to eq false
    end

    it 'should be false when password is blank' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'
      expect(user.valid?).to eq false
    end

    it 'should be false when short_bio is nil' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'
      user.password = 'a_secure_passWord!'
      user.short_bio = nil
      expect(user.valid?).to eq false
    end

    it 'should be false when short_bio is blank' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'
      user.password = 'a_secure_passWord!'
      user.short_bio = '  '
      expect(user.valid?).to eq false
    end

    it 'should raise error when time out' do
      user = described_class.first
      user.updated_on = 15.minutes.ago
      expect { described_class.verify_time? }.to raise_error(StandardError)
    end

    # rubocop: disable ExampleLength
    it 'should be true when all field are valid' do
      user.name = 'John Doe'
      user.email = 'john.doe@someplace.com'
      user.password = 'a_secure_passWord!'
      user.short_bio = 'Hi. I am John Doe'
      user.login_tries
      expect(user.valid?).to eq true
    end
    # rubocop: enable ExampleLength
  end

  describe 'authenticate' do
    let(:password) { 'password' }

    before :each do
      user.email = 'john.doe@someplace.com'
      user.password = password
    end

    it 'should raise error when password do not match' do
      email = user.email
      password = 'wrong_password'
      expect { described_class.authenticate(email, password) }.to raise_error(StandardError)
    end

    it 'should raise error when email do not match' do
      email = 'wrong@email.com'
      expect { described_class.authenticate(email, password) }.to raise_error(StandardError)
    end

    it 'should return the user when email and password match' do
      email = user.email
      expect(described_class).to receive(:first).with(email: email).and_return(user)

      authentication_result = described_class.authenticate(email, password)

      expect(authentication_result).to eq user
    end

    it 'should raise error when login_tries is 8 and block_date is now' do
      email = user.email
      expect(described_class).to receive(:first).with(email: email).and_return(user)

      user.login_tries = 8
      user.block_date = Time.zone.now
      expect { described_class.authenticate(email, password) }.to raise_error(StandardError)
    end

    it 'should return user when login_tries is 8 and block_date is 15/10' do
      expect(described_class).to receive(:first).with(email: user.email).and_return(user)
      user.login_tries = 8
      user.block_date = DateTime.new(2018, 10, 15, 12, 0, 0)
      authentication_result = described_class.authenticate(user.email, password)
      expect(authentication_result).to eq user
    end

    it 'should reset login_tries to 0 when block_date is 15/10' do
      expect(described_class).to receive(:first).with(email: user.email).and_return(user)
      user.login_tries = 8
      user.block_date = DateTime.new(2018, 10, 15, 12, 0, 0)
      authentication_result = described_class.authenticate(user.email, password)
      expect(authentication_result.login_tries).to eq user.login_tries
    end
  end
end
