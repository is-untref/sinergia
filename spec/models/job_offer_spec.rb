require 'spec_helper'

describe JobOffer do
  subject(:job_offer) { described_class.new }

  describe 'model' do
    it { is_expected.to respond_to(:id) }
    it { is_expected.to respond_to(:title) }
    it { is_expected.to respond_to(:location) }
    it { is_expected.to respond_to(:description) }
    it { is_expected.to respond_to(:owner) }
    it { is_expected.to respond_to(:owner=) }
    it { is_expected.to respond_to(:created_on) }
    it { is_expected.to respond_to(:updated_on) }
    it { is_expected.to respond_to(:is_active) }
    it { is_expected.to respond_to(:remuneration) }
  end

  describe 'valid?' do
    it 'should be false when title is blank' do
      job_offer.title = ''
      expect(job_offer).not_to be_valid
    end

    it 'should be false when remuneration is 1000009' do
      job_offer.remuneration = 1_000_009
      expect(job_offer).not_to be_valid
    end

    it 'should be false when remuneration is negative -1' do
      job_offer.remuneration = -1
      expect(job_offer).not_to be_valid
    end
  end

  describe 'deactive_old_offers' do
    let(:today_offer) do
      today_offer = described_class.new
      today_offer.updated_on = Date.today
      today_offer.is_active = true
      today_offer
    end

    let(:thirty_day_offer) do
      thirty_day_offer = described_class.new
      thirty_day_offer.updated_on = Date.today - 45
      thirty_day_offer.is_active = true
      thirty_day_offer
    end

    it 'should deactivate offers updated 45 days ago' do
      expect(described_class).to receive(:where)
        .with(is_active: true)
        .and_return([thirty_day_offer])
      described_class.deactivate_old_offers
      expect(thirty_day_offer.is_active).to eq false
    end

    it 'should not deactivate offers created today' do
      expect(described_class).to receive(:where).with(is_active: true).and_return([today_offer])
      described_class.deactivate_old_offers
      expect(today_offer.is_active).to eq true
    end
  end

  describe 'normalize labels offer' do
    it 'should be normalize label to lower' do
      job_offer.labels = 'LabelInLower'
      expect(job_offer.normalize_labels).to eq 'labelinlower'
    end

    it 'should be normalize label to lower without blanks' do
      job_offer.labels = 'Label, In , Lower  , without , blanks'
      expect(job_offer.normalize_labels).to eq 'label,in,lower,without,blanks'
    end

    it 'should be normalize label blanks and to convert label lower without blanks' do
      job_offer.labels = ' Label, In , Lower  ,  without     blanks  '
      expect(job_offer.normalize_labels).to eq 'label,in,lower,without blanks'
    end
  end
end
