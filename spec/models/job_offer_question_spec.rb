require 'spec_helper'

describe JobOfferQuestion do
  describe 'create_for' do
    it 'should set question and subject' do
      question = 'Are you from?'
      subject = 'Ruby & Python Ssr'
      joq = described_class.new('applicant@test.com', 'offerer@test.com', subject, question)
      expect(joq.question).to eq(question)
      expect(joq.subject).to eq(subject)
    end
  end

  describe 'send email to offerer' do
    it 'should deliver mail with question' do
      question = 'Hi. Are you from?'
      subject = 'Ruby & Python Ssr'
      joq = described_class.new('applicant@test.com', 'offerer@test.com', subject, question)
      expect(JobVacancy::App).to receive(:deliver).with(:offer_question, :offer_question_email, joq)
      joq.send_mail
    end
  end

  describe 'dont send mail when question is nil' do
    it 'dont send is true' do
      subject = 'Ruby & Python Ssr'
      joq = described_class.new('applicant@test.com', 'offerer@test.com', subject, nil)
      joq.send_mail
      expect(joq.dont_send).to be_truthy
    end
  end
end
