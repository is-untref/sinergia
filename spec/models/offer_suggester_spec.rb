require 'spec_helper'

describe 'suggester' do
  user_suggester = User.create(email: 'user_suggester@test.com',
                               name: 'user_suggester',
                               password: 'Sinergia2018!',
                               short_bio: 'Hi. I am Offerer for suggester test')
  ruby_label_offer = JobOffer.new
  ruby_label_offer.owner = user_suggester
  ruby_label_offer.updated_on = Date.today
  ruby_label_offer.is_active = true
  ruby_label_offer.title = 'ruby'
  ruby_label_offer.location = 'location'
  ruby_label_offer.description = 'description'
  ruby_label_offer.labels = 'ruby'
  ruby_rails_label_offer = JobOffer.new
  ruby_rails_label_offer.owner = user_suggester
  ruby_rails_label_offer.updated_on = Date.today
  ruby_rails_label_offer.is_active = true
  ruby_rails_label_offer.title = 'ruby rails'
  ruby_rails_label_offer.location = 'location'
  ruby_rails_label_offer.description = 'description'
  ruby_rails_label_offer.labels = 'ruby rails'
  ruby_sinergia_label_inactive_offer = JobOffer.new
  ruby_sinergia_label_inactive_offer.owner = user_suggester
  ruby_sinergia_label_inactive_offer.updated_on = Date.today
  ruby_sinergia_label_inactive_offer.is_active = false
  ruby_sinergia_label_inactive_offer.title = 'ruby and sinergia'
  ruby_sinergia_label_inactive_offer.location = 'location'
  ruby_sinergia_label_inactive_offer.description = 'description'
  ruby_sinergia_label_inactive_offer.labels = 'ruby,sinergia'
  ruby_sinergia_label_inactive_offer.save
  ruby_label_offer.save
  ruby_rails_label_offer.save

  it 'should return ruby rails offer when i apply to  ruby offer' do
    results = OfferSuggester.get_suggest(ruby_label_offer)
    result = results.detect { |hash| hash[:title] == 'ruby rails' }
    expected_result = result.title.include? 'ruby rails'
    ruby_rails_label_offer.destroy
    expect(expected_result).to eq true
  end

  it 'should not return ruby and sinergia offer since this offer is not active' do
    results = OfferSuggester.get_suggest(ruby_label_offer)
    result = results.detect { |hash| hash[:title] == 'ruby and sinergia' }
    expected_result = result.nil?
    ruby_sinergia_label_inactive_offer.destroy
    expect(expected_result).to eq true
  end
end
