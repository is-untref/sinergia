require 'spec_helper'
require 'rspec'
require_relative '../../models/password_validator'

describe 'validator' do
  subject(:password_validator) { PasswordValidator.new }

  it 'When i pass S1nerg*a like a password then the validator return error since this password its too short' do
    expect { password_validator.validate_long('S1nerg*a') }.to raise_error(ExceptionShortPassword)
  end

  it 'When i pass S1Nerg*ateam like a password then the validator return error since this password doesnt contains numbers' do
    rubocop = ExceptionPasswordWithoutNumbers
    expect { password_validator.validate_numbers('SINerg*ateam') }.to raise_error(rubocop)
  end

  it 'When i pass S1NERG*ATEAM like a password then the validator return error since this password doesnt contains lowercases' do
    rubocop = ExceptionPasswordWithoutLowercase
    expect { password_validator.validate_lowercases('S1NERG*ATEAM') }.to raise_error(rubocop)
  end

  it 'When i pass s1nerg*ateam like a password then the validator return error since this password doesnt contains uppercases' do
    rubocop = ExceptionPasswordWithoutUppercase
    expect { password_validator.validate_uppercases('s1nerg*ateam') }.to raise_error(rubocop)
  end

  it 'When i pass S1nergiateam like a password then the validator return error since this password doesnt contains symbols' do
    rubocop = ExceptionPasswordWithoutSymbols
    expect { password_validator.validate_symbols('S1nergiateam') }.to raise_error(rubocop)
  end

  it 'When i pass Sinergia2018! like a password then the validator return true since the password its valid' do
    expect(password_validator.validate('Sinergia2018!')).to be_truthy
  end
end
