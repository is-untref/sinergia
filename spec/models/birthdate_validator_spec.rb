require 'spec_helper'

describe 'validator' do
  params_too_young = { 'birthdate' => '2018-01-01' }
  params_future = { 'birthdate' => '2019-01-01' }
  subject(:birthdate_validator) { BirthdateValidator.new }

  it 'When i pass a 2019-01-01 like birthdate, the validator return error since this birthdate cant be future' do
    e = ExceptionFutureBirthdate
    expect { birthdate_validator.validate(params_future['birthdate']) }.to raise_error(e)
  end

  it 'When i pass a 2018-01-01 like birthdate, the validator return error since the user doens have requerired age' do
    e = ExceptionYoungUser
    expect { birthdate_validator.validate(params_too_young['birthdate']) }.to raise_error(e)
  end
end
